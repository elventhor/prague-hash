<?php
session_start();
# Common functions
$loaded = "";
foreach (glob("functions/*.php") as $filename)
{
    require_once($filename);
    $loaded .= $filename." ";
}
debug("Loaded $loaded");
unset($loaded);
unset($filename);
# Set up variables and such
foreach (glob("conf/*.php") as $filename)
{
    require_once($filename);
    debug("Loaded $filename");
}
unset($filename);
# We should now have $db and $smarty up and running..

# Handle ajax
if(isset($_REQUEST['ajax']))
{
	# This is the only time we don't run the scope to the end. ajaxHandler ends the script.
	require_once('handlers/ajaxHandler.php');
}
# Handle forms
if(isset($_POST['form']))
{
	require_once('handlers/formHandler.php');
}
# Handle pages
require_once('handlers/pageHandler.php');

$smarty->assignByRef('hashers',getHashers());
$smarty->assignByRef('news',getNews());
$smarty->assignByRef('nexthashes',getNextHashes());
$smarty->assignByRef('prevhashes',getPrevHashes());
$smarty->assignByRef('Hashers',getHashersById());
// Hashes = upcoming hashes
// News = newsitems

# Assign smarty variables
$smarty->assignByRef('messages',$message);
$smarty->assign('title','Prague Hash House Harriers');
if($_SERVER['REMOTE_ADDR'] == getHostByName("stfu.elventhor.net")) {
#	$smarty->assignByRef('Debug',$Debugdata);
#	$smarty->assignByRef('debug',$settings['debugger']);
}
for($i = 0; $i < 24; $i++) {
	$hours[] = $i;
}
$smarty->assignByRef('hours',$hours);
$smarty->assignByRef('user',$user);
# Show page
$Core = 'Core.tpl';
$smarty->display($Core);
