<?php
if(isset($_REQUEST['id'])) {
	$id = intval($_REQUEST['id']);
	$newsItem = getNewsItem($id);
	if(empty($newsItem)) { $page = 'error/404';} else {
		$page = "newsItem";
		$smarty->assignByRef('newsItem',$newsItem);
	}
} else {
	$page = "news";
	$smarty->assignByRef("allnews",getNews(99));
}
