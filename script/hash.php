<?php
$id = intval($_REQUEST['id']);
$hash = getHash($id);
if(!empty($user)) {
	if($user['admin']) {
		$hash['is_admin'] = 1;
	}
	if(in_array($user['id'],$hash['hares'])) {
		$hash['is_hare'] = 1;
	}
	if(in_array($user['id'],$hash['scribes'])) { 
		$hash['is_scribe'] = 1;
	}
	foreach($hash['hashers'] as $k => $v) {
		if($user['id'] == $v['hasher']) {
	                $hash['is_hashing'] = 1;
		}
	}
	foreach($hash['hares'] as $hare) {
		$hash['haresbyid'][$hare] = true;
	}
        foreach($hash['scribes'] as $scribe) {
                $hash['scribesbyid'][$scribe] = true;
        }

}
        foreach($hash['hashers'] as $k => $v) {
		$hashers[] = $v['hasher'];
        }
	$hash['hashers'] = $hashers;

if(empty($hash)) { $page = 'error/404'; } else {
	$page = 'hash';
	$smarty->assignByRef('hash',$hash);
}
