          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Upcoming Hashes</li>
  <!--            <li class="active"><a href="#">Link</a></li>-->
		{foreach from=$nexthashes item=hash}
              <li><a href="/hash/{$hash.id}">Hash {$hash.number} {if $hash.name} / {$hash.name}{/if}</a></li>
		{/foreach}
              <li class="nav-header">Previous Hashes</li>
                {foreach from=$prevhashes item=hash}
              <li><a href="/hash/{$hash.id}">Hash {$hash.number} {if $hash.name} / {$hash.name}{/if}</a></li>
                {/foreach}

              <li class="nav-header">News</li>
                {foreach from=$news item=new}
              <li><a href="/news/{$new.id}">{$new.title}</a></li>
		{/foreach}
              <li class="nav-header">Info</li>
              <li><a href="/about/">About us</a></li>
              <li><a href="/visitors/">Visitors</a></li>
              <li><a href="/history/">History</a></li>
              <li><a href="/shop/">Haberdashery</a></li>
              <li><a href="/photos/">Photos</a></li>
              <li><a href="/contacts/">Contacts</a></li>
              <li><a href="/breweries/">Breweries</a></li>

              <li class="nav-header">Random hashers</li>
		{foreach from=$hashers item=hasher}
              <li><a href="/hasher/{$hasher.id}">{$hasher.name}</a></li>
		{/foreach}
            </ul>
          </div><!--/.well -->

