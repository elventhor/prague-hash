<div class="span9">
<h1>Information for Visitors</h1>
<div class="hashText">We usually start and finish from public
transport, so there usually isn't anywhere to change before the hash, or
store items during the hash. Most people arrive ready for the event. We
are not the quickest hash in the world, and almost always have a fair
contingent of walkers. After the On-In we typically go to a restaurant
and have a meal. The cost of the hash is 80Czk.</div>
<div class="hashText">We have a range of hash shirts and other bits. 
Let us know if you're interested BEFORE the event so we can bring a stuff along.</div>
<div class="hashHeader">About Prague</div>
<div class="hashText">Prague is a popular holiday destination,
combining excellent beer, stylish architecture, hidden parks and
gardens, and great cultural and historical attractions all of which
you will experience on our hash trails. <br />
Getting to Prague is not a problem with a mixture of low-cost airlines
and international carriers coming to our shiny and efficient airport on
the north-western edge of the city.  Typically, cheaper prices of course if you book earlier. </div>
<div class="hashText">CASH: Lots of cashpoint / ATM (bankomat) machines at the airport and
throughout the city to withdraw Czech crowns (CZK). Approximate exchange
rates are CZK 27 = EUR 1. CZK 35 = GBP 1. CZK 25 = USD 1. (Approx rates jan. 2016)
</div>
<div class="hashHeader">Transport</div>
<div class="hashText">Getting into the city centre from the
airport<br />
Bargain approach is a CZK 32 ticket (about EUR 1) for public bus No.119
going every 10 minutes to Nadrazi Veleslavin metro and then take the green metro
line into the city centre (about 40 minutes total). Another option for
CZK 32 is bus no. 100 to Zlicin at the end of the yellow metro line
connecting to the city centre. There are ticket machines and a public
transport info desk at airport which sells tickets, maps and guides
Other options are taxi stands with meter or fixed price zones (taxi EUR
20+), or minibuses to Namesti Republiky in the centre see Prague
(Ruzyne) airport sites www.csl.cz/en or www.prg.aero for lots of links,
info, maps and stuff.</div>
<div class="hashText">Getting around Prague other than walking through the
pedestrian-friendly city Excellent public transport options on metro,
tram and bus masses of info at <a target="_blank" href="http://www.dpp.cz/en/">dpp.cz</a> ( in English, Deutsch).
Tickets from yellow machines cost CZK 26 for 75 minutes or CZK 18 for
short journeys; also CZK 100 for one-day. Tickets also from newsagents,
public transport info offices at airport, or send SMS with message DPT
to 902026. Don't risk traveling without a ticket it's not worth the
hassle and embarrassment (to all the other passengers as well as
yourself!). Watch your valuables and pockets carefully, as Prague has a
number of experienced pick-pockets.</div>
<div class="hashText">Taxi: For taxi, there's <a href="http://www.nejlevnejsi-taxi.cz/en/">Nejlevnejsi taxi (cheap and good)</a>, <a href="http://www.aaataxi.cz/en/">AAA-taxi (expensive, might cheat, more cars)</a> and Uber. Always call/order the taxi, never take a taxi from the street, you will get scammed. </div>
<div class="hashHeader">General</div>
<div class="hashText">General / Tourist Information - below is a
list of helpful websites, many of which also offer accommodation info /
links:
<ul>
	<li>www.inyourpocket.com is simply marvelous. PDF downloads on
	Prague - City Guide and mini Instant Guide</li>
	<li>www.pis.cz - Prague Information Service (official tourist
	info)</li>
	<li>www.czechtourism.com  - official site of the Czech Tourist
	Authority</li>
	<li>www.expats.cz - info for living in Prague</li>
	<li>www.timeout.com/prague - good overview of tourist sights</li>
	<li>www.praguepost.com - Prague's weekly English-language
	newspaper, incl. restaurant and culture listings</li>
</ul>
</div>
<div class="hashHeader">Accommodation</div>
<div class="hashText">Hashers visiting Prague over the years have
stayed at the following hostels: www.arpacayhostel.cz and
www.akatpension.com - both are in Smichov, Prague 5, close to each other
and only 7 mins by tram to city centre For those cheap hashers amongst
you, try the Golden Sickle hostel: very cheap and right in centre of
town on Vodickova: www.goldensickle.com: mostly dormitory sleeping Also
coming recommended are www.miss-sophies.com and www.czech-inn.com. Hotels
in Prague, like everywhere else, range in price and standard - one
option: Hotel U Krize www.ukrize.com </div>
<div class="hashText">
Below are some on-line hotel listing and reservation services:
<ul>
	<li>www.prague30.com - EUR 30 pppn</li>
	<li>www.marys.cz - hotels, BandBs, etc</li>
	<li>www.hostel.cz - hostels, obviously</li>
	<li>www.travellers.cz - hostels</li>
	<li>www.abaka.com - apartments, hotels</li>
	<li>www.sirtobys.com - hostel-style</li>
	<li>www.avetravel.cz - agency</li>
	<li>www.hotels.source.cz</li>
	<li>www.gomio.com - hostels</li>
</ul>
</div>
<div class="hashText">Pubs, restaurants and other food - so many options - beer everywhere at
any hostinec or hospoda - look at the In Your Pocket
guide above. We like Pivovarsky Dum. www.gastroinfo.cz/pivodum for
micro-brewery beers. Also Tesco, Albert and lots of other grocery shops
throughout the city for water, beer and other essentials.</div>
</div> 
