{literal}
<script type="text/javascript">
$(function() {
	$('#delete_file').click(function() {
		confirm("Are you sure that you want to delete file "+$(this).data("filename"));
	});
	$('#hash_admin_title').click(function() {
		$('#hash_admin').toggle();
	});
        $('#hash_scribe_title').click(function() {
                $('#hash_scribe').toggle();
        });

});
</script>
{/literal}
<div class="span9">
<!-- Template page for a hash -->
<h1>Hash {$hash.number} {if $hash.name}/ {$hash.name}{/if}</h1>
<div>{if $hash.is_admin}You are admin<br />{/if}{if $hash.is_hare}You are hare<br />{/if}{if $hash.is_scribe}You are scribe!<br />{/if}</div>
{if $user.id}
{if $hash.is_hare}You are the hare!{else if $hash.is_hashing}
 <form method="post" action="">
 <input type="hidden" name="form" value="leaveHash" />
 <input type="hidden" name="hash_id" value="{$hash.id}" />
 <input type="submit" name="button" value="Leave hash" />
 </form>
 {else}
 <form method="post" action="">
 <input type="hidden" name="form" value="joinHash" />
 <input type="hidden" name="hash_id" value="{$hash.id}" />
 <input type="submit" name="button" value="Join hash" />
 </form>
{/if}
{/if}
<div class="details">
    Date &amp; time: {$hash.date}<br />
    Location: <a target="_new" href="https://google.com/maps/search/{$hash.location}">{$hash.location}</a><br />
    Hare(s): {foreach from=$hash.hares item=i}<a href="/hasher/{$i.id}">{$Hashers[$i.id].name}</a> {/foreach}<br />
    Scribe(s): {foreach from=$hash.scribes item=i}<a href="/hasher/{$i.id}">{$Hashers[$i.id].name}</a> {/foreach}<br />
    Hash Type: {$hash.type}<br />
</div>
<div class="url"><a href="{$hash.url}">External url / facebook event</a></div>
<div class="hashers">Attending hashers: {foreach from=$hash.hashers item=i}<a href="/hasher/{$i.id}">{$Hashers[$i.id].name}</a> {/foreach}</div>
<div class="visitors">Visitors: {$hash.visitors}</div>
<div class="description">Description: {$hash.description}</div>
<div class="scribe">Scripture: {$hash.scribe}</div>
{if $user.id}
<div class="photos">
<h2>Photos!</h2>
{if !$hash.photos}<p>No photos yet :/ Upload them!</p>{/if}
{foreach from=$hash.photos item=i}
	<a href="/images/hashes/{$hash.id}/{$i}" data-title="{$i.filename}" data-lightbox="hash-{$hash.id}"><img width="100px" height="100px" src="/images/hashes/{$hash.id}/{$i}" /></a>{if $hash.is_admin}{/if}
{/foreach}
<p>
<div id="filelist"></div>
<br />
 
<div id="container">
    <span id="message"></span>
    <a id="pickfiles" href="javascript:;">[Select photos to upload]</a>
</div>
<script type="text/javascript">
var hash_id = "{$hash.id}";
{literal}
var uploader = new plupload.Uploader({
    runtimes : 'html5,flash,silverlight,html4',

    browse_button : 'pickfiles', // you can pass in id...
    container: document.getElementById('container'), // ... or DOM Element itself

    url : "/?ajax=uploadPhoto&hash_id="+hash_id,

    filters : {
        max_file_size : '10mb',
        mime_types: [
            {title : "Image files", extensions : "jpg,gif,png"}
       ]
    },

    // Flash settings
    flash_swf_url : '/js/Moxie.swf',

    // Silverlight settings
    silverlight_xap_url : '/js/Moxie.xap',


    init: {
/*
        PostInit: function() {
            document.getElementById('filelist').innerHTML = '';

            document.getElementById('uploadfiles').onclick = function() {
                uploader.start();
                return false;
            };
        },
*/
        FilesAdded: function(up, files) {
//            plupload.each(files, function(file) {
 //               document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
		$('#pickfiles').hide();
		$('#message').html('Uploading pictures..');
		uploader.start();
//            });
        },

        UploadProgress: function(up, file) {
		$('#message').html("Uploading " + file.name + " " + file.percent + "%");
 //           document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
        },

        Error: function(up, err) {
            document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
        },
	UploadComplete: function() {
		location.reload();
	}
    }
});

uploader.init();

</script>
{/literal}
 
<br />

</div>
{else}
<p>You need to be logged in to see/upload photos!</p>
{/if}
</div>
{if $hash.is_admin || $hash.is_hare}
<span class="span9">
<h1 class="link" id="hash_admin_title">Hash admin (click to extend)</h1>
<div id="hash_admin" style="display: none;">
<form method="post" action="">
<input type="hidden" name="form" value="updateHash" />
<input type="hidden" name="hash_id" value="{$hash.id}" />

<input type="text" name="number" placeholder="Next number: {$nextHashNumber}" id="hash_number" value="{$hash.number}" /><label for="hash_number">Hash number</label><br />
<input type="text" name="name" placeholder="Optional hash name" id="hash_name" value="{$hash.name}" /><br />
<input type="text" name="location" placeholder="Hash location" value="{$hash.location}"/><br />
<input type="date" name="date" alt="Hash date" value="{$hash.day}" /> Hash date<br />
<input type="text" name="hour" alt="Hash hour" value="{$hash.hour}" /><input type="text" name="minute" alt="Hash minute" value="{$hash.minute}" /><br />
<select name="type">
        <option value="A-B" {if $hash.type == "A-B"} selected=true {/if}>A-B</option>
        <option value="A-A" {if $hash.type == "A-A"} selected=true {/if}>A-A</option>
</select><br />
<select name="hares[]" multiple>
        {foreach from=$Hashers item=h}
                <option value="{$h.id}" {if $hash.haresbyid[$h.id]} selected="true" {/if}>{$h.name} ({$h.realname})</option>
        {/foreach}
</select> Hare(s)<br />
<select name="scribes[]" multiple>
        {foreach from=$Hashers item=h}
                <option value="{$h.id}" {if $hash.scribesbyid[$h.id]} selected="true" {/if}>{$h.name} ({$h.realname})</option>
        {/foreach}
</select> Hash Scribe(s)<br />
<textarea name="description" placeholder="Hash description" rows="10">{$hash.description}</textarea><br />
<input type="text" name="url" placeholder="External url / facebook event" value="{$hash.url}" /><br />
<input type="text" name="visitors" placeholder="Visitors" value="{$hash.visitors}" /><br />
<input type="submit" name="submit" value="Update hash" />
</form>
</div>
</span>
{/if}
{if $hash.is_scribe}
<span class="span9">
<h2 id="hash_scribe_title">Hash scribe (click to extend)</h1>
<div id="hash_scribe" style="display:none;">
<form method="post" action="">
<input type="hidden" name="form" value="hash_scribe" />
<input type="hidden" name="hash_id" value="{$hash.id}" />
<textarea name="scripture" rows="30" placeholder="Please type your scripture here!">{$hash.scribe}</textarea><br />
<input type="submit" name="submit" value="Save scripture" />
</form>
</div>
</span>
{/if}
