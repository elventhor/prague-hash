{literal}
<script type="text/javascript">
$(function() {
	$('#newhash_btn').click(function() {
		$('#newhash').toggle();
	});
        $('#newsitem_btn').click(function() {
                $('#newsitem').toggle();
        });

});
</script>
{/literal}
<div class="span9">
<h1>General admin</h1>

<h2 class="link" id="newhash_btn">New hash</h2>
<div id="newhash" style="display:none;">
<form method="post" action="">
<input type="hidden" name="form" value="createHash" />
<input type="text" name="number" placeholder="Next number: {$nextHashNumber}" id="hash_number" /><label for="hash_number">Hash number</label><br />
<input type="text" name="name" placeholder="Optional hash name" id="hash_name" /><br />
<input type="text" name="location" placeholder="Hash location" /><br />
<input type="date" name="date" alt="Hash date" />
<select name="hour" style="width: 75px;">
	<option value="0">Hour</option>
{foreach from=$hours item=i}
	<option value="{$i}" {if $i == 14}selected=true{/if}>{$i}</option>
{/foreach}
</select>
<select name="minute" style="width: 100px;">
	<option value="0">Minutes</option>
        <option value="0">00</option>
	<option value="15">15</option>
        <option value="30">30</option>
        <option value="45">45</option>
</select><br />
<select name="type">
	<option value="A-B">A-B</option>
	<option value="A-A">A-A</option>
</select><br />
<select name="hares[]" multiple style="width: 250px;">
	<option value="">No hare yet</option>
	{foreach from=$Hashers item=h}
		<option value="{$h.id}">{$h.name} ({$h.realname})</option>
	{/foreach}
</select> Hare<br />
<textarea name="description" placeholder="Hash description" rows="10"></textarea><br />
<input type="checkbox" checked="true" name="send_email" /> Send email to all hashers about this hash?<br /><br />
<p><input type="submit" value="Create hash" /></p>
</form>
</div>
<h2 class="link" id="newsitem_btn">Newsitem</h2>
<div id="newsitem" style="display:none;">
<p>
<input type="text" name="title" value="" placeholder="Newsitem title" /><br />
<input type="date" name="date" id="newsitem_date" /><label for="newsitem_date">Publish date</label><br />
<textarea name="content" rows="10" placeholder="Newsitem content"></textarea><br />
<input type="button" value="Create newsitem" />
</div>
</div>
