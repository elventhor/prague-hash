{literal}
<script type="text/javascript">
$(function() {
	$('#reg').click(function() {
		if( $('#email').val() == "") {
			alert("Empty email! Fail!"); 
			return;
		}
		var res = prompt("Please type: on on");
		if(res != "on on") {
			alert("You failed to type on on");
		} else {
			$.post('/',{ajax: "register", email: $('#email').val()},function(data) {
				console.log(data);
				alert(data.message);
			});
		}
	});
});
</script>
{/literal}
<div class="span9">
<h2>Register to the Prague Hash House Harriers</h2>
<p>Why register?</p>
<p>By registering you get access to the hash pages and photos you have been on, you get the ability to register to hashes, an option to hare a hash and emails about new hashes. You <b>don't need to register to come to a hash</b>. You get an email with further instructions upon registering.</p>
<input type="text" name="email" id="email" placeholder="Your email address" />
<input type="submit" name="submit" value="register" id="reg" />
