{literal}
<script type="text/javascript">
$(function() {
	$('#edit_profile_title').click(function() {
		$('#edit_profile').toggle();
	});
});
</script>
{/literal}
<div class="span3">
	<h1>{$hasher.name}</h1>
	<p>Bio: {$hasher.bio}</p>
	<p>Attended {$hasher.hashcount} hashes!</p>
	{if $hasher.hashes[0].id}
	<p>Last seen on <a href="/hash/{$hasher.hashes[0].id}">hash {$hasher.hashes[0].number}</a></p>
	{/if}
</div>
<div class="span3">
	<p><img onError="this.src='/images/hashers/nophoto.jpg';" src="/images/hashers/{$hasher.id}.jpg" alt="hasher" /></p>
</div>

{if $hasher.id == $user.id}
<div class="span3">
<h1 class="link" id="edit_profile_title">Click to edit your profile!</h1>
<div id="edit_profile" style="display: none;">
        <form method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="form" value="editHasher" />
        <input type="text" name="name" placeholder="Hash name" value="{$hasher.name}" />Please only fill if you have been given a hash name, otherwise use Just Firstname<br />
        <input type="text" name="realname" placeholder="Real name" value="{$hasher.realname}" /> Your real name, shown only for admins for times when it's needed (canoe hash reg etc)<br />
        <input type="text" name="email" placeholder="Your email" value="{$hasher.email}" /><br />
        <input type="checkbox" value="1" {if $hasher.email_hash}checked=true{/if} name="email_hash" id="email_hash" /><label for="email_hash">Send me emails about upcoming hashes</label>
        <input type="checkbox" value="1" {if $hasher.email_news}checked=true{/if} name="email_news" id="email_news" /><label for="email_news">Send me emails about news</label>
        <textarea rows="10" name="bio" placeholder="Hash bio">{$hasher.bio}</textarea><br />
        <input type="password" name="newpassword" placeholder="New password" />Fill only if you want to change your password!<br />
        <input type="file" name="hash_photo" />Change your hash photo<br />
        <input type="submit" name="submit" value="Save data" />
</div>
<div class="span3">
        <p><img onError="this.src='/images/hashers/nophoto.jpg';" src="/images/hashers/{$hasher.id}.jpg" alt="hasher" /></p>
</div>
</form>
</div>
{/if}

