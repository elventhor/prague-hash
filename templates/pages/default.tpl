        <div class="span9">
          <div class="hero-unit">
            <h1>Welcome to Prague Hash!</h1>
		<p>This is a dashboard page with last / next hashes, news and other things like that</p>
		<img src="/images/h3logo.gif" />
          </div>
          <div class="row-fluid">
            <div class="span4">
              <h2>Previous hash: {$prevHash.number}</h2>
		<p>{$prevHash.description}</p>
              <p><a class="btn" href="/hash/{$prevHash.id}">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="span4">
              <h2>Next hash: {$nextHash.number}</h2>
		<p>{$nextHash.description}</p>
              <p><a class="btn" href="/hash/{$nextHash.id}">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="span4">
              <h2>News: {$newNews.title}</h2>
		<p>{$newNews.content}</p>
              <p><a class="btn" href="/news/{$newNews.id}">More &raquo;</a></p>
            </div><!--/span-->
          </div><!--/row-->
