{literal}
<script type="text/javascript">
$(function() {
	$('#forgotten-btn').click( function() {
		$('#forgotten').toggle();
	});
});
</script>
{/literal}
<div class="span9">
 <h1>Prague H3 login</h1>
 <form method="post" name="login">
  <input type="hidden" name="form" value="login" />
  <input type="email" name="email" placeholder="email" /><br />
  <input type="password" name="pass" placeholder="password" /><br />
  <input type="submit" name="submit" value="Login" /><br />
 </form>
 <p id="forgotten-btn">Forgotten password?</p>
 <div style="display:none;" id="forgotten">
	<form method="post" action="">
	<input type="hidden" name="form" value="resetPassword" />
	<input type="email" name="email" placeholder="Your email address" /><br />
	<input type="submit" name="button" value="Reset password" /></div>
	</form>
 <p><a href="/register/">Register</a></p>
</div>
