<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>	
  <title>{$title}</title>
  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
  <meta http-equiv="Content-Language" content="fi" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script src="/js/jquery.js"></script>
  <link rel="stylesheet" type="text/css" href="/css/default.css" />
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/lightbox.css" rel="stylesheet">


    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">
   {if $user.id}
	<script type="text/javascript" src="/js/plupload.full.min.js"></script>

   {/if}
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  {foreach from=$head item=headitem}
   {$headitem}
  {/foreach}
 </head>
    {include file="Navi.tpl"}

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
	 {include file="Sidebar.tpl"}
        </div><!--/span-->
	{if $messages}
		<div class="span9">
		{foreach from=$messages item=i}
			<h2>{$i}</h2>
		{/foreach}
		</div>
	{/if}
	{include file="pages/$page.tpl"}
      </div><!--/row-->

      <hr>

      <footer>
        <p style="display:none;">Prague Hash House Harriers</p>
	{if $debug}<p>{$Debug}</p>{/if}
      </footer>

    </div><!--/.fluid-container-->
    <script src="/js/lightbox.js"></script>

  </body>
</html>
