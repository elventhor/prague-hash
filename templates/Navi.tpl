    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/">Prague Hash</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
		{if $user.id}
              Logged in as <a href="/hasher/{$user.id}" class="navbar-link">{if $user.name}{$user.name}{else $user.email}{/if}</a> | <a href="/logout/">Log out</a>
		{else}
		<a href="/login/">Log in</a> or Facebook
		{/if}
            </p>
            <ul class="nav">
		{if $user.admin}
              <li><a href="/admin/">Admin</a></li>
              <li><a href="/users/">Users</a></li>

		{/if}
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

