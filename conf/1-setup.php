<?php
$settings = array();
# Setup
$settings['debugger'] = 1;
$settings['dev'] = 1;

# Debug constants
define('DEBUG_INFO',0);
define('DEBUG_WARN',1);
define('DEBUG_ERR',2);
define('DEBUG_SQL',3);
define('DEBUG_SQL_ERR',4);
define('DEBUG_FATAL',5);
define('DEBUG_LOG',6);
