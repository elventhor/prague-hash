<?php
require_once('smarty/Smarty.class.php');
$smarty =& new Smarty;
$smarty->template_dir = 'templates/';
$smarty->compile_dir = 'cache/templates_c/';
$smarty->config_dir = 'conf/smarty/';
$smarty->cache_dir = 'cache/smarty/';
