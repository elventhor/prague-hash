<?php
/**
 * Debug function
 * @global string $Debugdata contains all debug traces
 * @param mixed $data The debug data, number,string or an array of those
 * @param int $level The debug level, optional
*/
function debug($debug = "",$level = DEBUG_INFO)
{
	global $Debugdata;
	if(is_array($debug))
	{
		foreach($debug as $k => $v)
		{
			$debugstring = "$k => $v<br />\n";
		}
	} else
	{
		$debugstring = $debug;
	}
	switch($level)
	{
		case DEBUG_INFO:
			$Debugdata .= "INFO: $debugstring<br />\n";
			break;
		case DEBUG_WARN:
			$Debugdata .= "<b>WARN: $debugstring<br />\n";
			break;
		case DEBUG_ERR:
			$Debugdata .= "<b>ERROR: $debugstring<br />\n";
			break;
		case DEBUG_FATAL:
			$Debugdata .= "<b>FATAL: $debugstring<br />\n";
			die($Debugdata);
			break;
		case SQL_INFO:
			$Debugdata .= "SQL: $debugstring<br />\n";
			break;
		case SQL_ERR:
			$Debugdata .= "<b>SQL ERROR: $debugstring<br />\n";
			break;
		default:
			$Debugdata .= "Unknown debug level: $debugstring<br />\n";
			break;
	}
}
