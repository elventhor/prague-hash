<?php
function ajaxEnd($data) {
	header("Content-Type: text/json");
	die(json_encode($data));
}
function ajaxSuccess($msg,$data = array()) {
	$data['message'] = $msg;
	$data['status'] = 0;
	ajaxEnd($data);
}
function ajaxFail($msg,$data = array()) {
	$data['message'] = $msg;
	$data['status'] = 1;
	ajaxEnd($data);
}
