<?php
function updateHash($hash) {
	debug("updateHash called");
	$date = $hash['date']." ".$hash['hour'].":".$hash['minute'];
	db_update('UPDATE hashes SET
		hares = "'.$hash['hares'].'",
		date = "'.$date.'",
		location = "'.$hash['location'].'",
		type = "'.$hash['type'].'",
		description = "'.$hash['description'].'",
		number = "'.$hash['number'].'",
		name = "'.$hash['name'].'",
		scribes = "'.$hash['scribes'].'",
		visitors = "'.$hash['visitors'].'",
		url = "'.$hash['url'].'"
	WHERE id = '.$hash['hash_id']
	);
}
function createHash($hash) {
	/*
| id          | int(10) unsigned | NO   | PRI | NULL              | auto_increment |
| modified    | timestamp        | NO   |     | CURRENT_TIMESTAMP |                |
| hares       | text             | YES  |     | NULL              |                |
| date        | datetime         | YES  |     | NULL              |                |
| location    | text             | YES  |     | NULL              |                |
| type        | varchar(255)     | YES  |     | NULL              |                |
| description | text             | YES  |     | NULL              |                |
| scribes     | text             | YES  |     | NULL              |                |
| scribe      | text             | YES  |     | NULL              |                |
| number      | int(11)          | YES  |     | NULL              |                |
| name        | varchar(255)     | YES  |     | NULL              |                |
| visitors    | text             | YES  |     | NULL              |                |
| url         | varchar(255)     | YES  |     | NULL              |                |

	*/
        $date = $hash['date']." ".$hash['hour'].":".$hash['minute'];
	$id = db_insert('INSERT INTO hashes (hares, date, location, type, description, number, name) VALUES (
		"'.$hash['hares'].'",
                "'.$date.'",
                "'.$hash['location'].'",
                "'.$hash['type'].'",
                "'.$hash['description'].'",
                "'.$hash['number'].'",
                "'.$hash['name'].'"

	)');
	$h = implode($hash['hares']);
	foreach($h as $ha) {
		$hares[] = db_fetch_one('SELECT name FROM hashers WHERE id = '.$ha);
		$hares = implode(',',$hares);
	}
	if(!empty($id)) {
		$hashers = db_fetch_all('SELECT * FROM hashers WHERE email_hash = 1 AND email != ""');
		$subject = "[PRAGUE H3] Welcome to Hash #".$hash['number']." ".$hash['name'];
		$url = "http://".$_SERVER['HTTP_HOST'].'/hash/'.$id;
		$body = "Welcome to hash #".$hash['number']."!\nThe hash will be ".$hash['type'].", starting from ".$hash['location']." at ".$date."\n\nHare(s):\n$hares\n\n".
		$hash['description']."\n\nYou can just reply 'cuming' to this email to register yourself or check out the hash page at $url\n";
		foreach($hashers as $hasher) {
			mail($hasher['name']." <".$hasher['email'].'>',$subject,$body,'From: Prague Hash Mismanagement <hash@elventhor.net>');
		}
	}
	return $id;
}
function escape($string) {
	if(is_array($string)) { $string = serialize($string); }
	return mysql_real_escape_string($string,db_connect());
}
function email_exists($email) {
	$cnt = db_fetch_one('SELECT count(*) FROM hashers WHERE email = "'.escape($email).'"');
	if($cnt > 0) {return true; }
	$cnt = db_fetch_one('SELECT count(*) FROM register WHERE email = "'.escape($email).'"');
	return $cnt;
}
function sendRegistrationEmail($email) {
	$hash = md5(date('U')."paskaa kaiketi");
        $url = "http://".$_SERVER['HTTP_HOST'].'/register/'.$hash;
	db_insert('INSERT INTO register (email, ip, hash) VALUES ("'.escape($email).'","'.$_SERVER['REMOTE_ADDR'].'","'.$hash.'")');
	mail("Unknown Hasher <".escape($email).">","Welcome to the Prague Hash House Harriers!","Hi and welcome to the Prague Hash House Harriers! Please click the link below to proceed with your registration!\n$url\nOn on, PH3 Admins\n",'From: Hash Admins <hash@elventhor.net>');
}
function getNextHashNumber() {
	$current = db_fetch_one('SELECT max(number) FROM hashes');
	$current++;
	return $current;
}
function getHashersById() {
	$hashers = db_fetch_all('SELECT * FROM hashers');
	foreach($hashers as $hasher) {
		unset($hasher['pwhash']);
		$result[$hasher['id']] = $hasher;
	}
	return $result;
}
function getNews($cnt = 5) {
	return db_fetch_all('SELECT * FROM news WHERE published <= now() ORDER BY published DESC LIMIT '.$cnt);
}
function getNewsItem($id = false) {
	if(empty($id)) { return array(); }
	$id = intval($id);
	return db_fetch_row('SELECT * FROM news WHERE id = '.$id);
}
function getHashers($cnt = 5) {
	return db_fetch_all('SELECT * FROM hashers ORDER BY RAND() LIMIT '.$cnt);
}
function getHasher($id = false) {
        if(empty($id)) { return array(); }
        $id = intval($id);
        $hasher = db_fetch_row('SELECT * FROM hashers WHERE id = '.$id);
	unset($hasher['pwhash']);
	$hasher['hashes'] = db_fetch_all('SELECT hashes.* FROM hashes, hash_hashers WHERE hash_hashers.hasher = '.$id.' AND hash_hashers.hash = hashes.id AND hashes.date < now() AND hashes.number > 0');
	$hasher['hashcount'] = count($hasher['hashes']);
	if(empty($hasher['name'])) { $hasher['name'] = "Just Hasher"; }
	return $hasher;

}
function getNextHashes($cnt = 5) {
        return db_fetch_all('SELECT * FROM hashes WHERE date > now() AND hashes.number > 0 ORDER BY date ASC LIMIT '.$cnt);
}
function getPrevHashes($cnt = 5) {
        return db_fetch_all('SELECT * FROM hashes WHERE date < now() AND hashes.number > 0 ORDER BY date DESC LIMIT '.$cnt);
}
function getHash($id) {
        if(empty($id)) { return array(); }
        $id = intval($id);
        $hash = db_fetch_row('SELECT * FROM hashes WHERE id = '.$id);
	$d = explode(' ',$hash['date']);
	$hash['day'] = $d[0];
	$d = explode(':',$d[1]);
	$hash['hour'] = $d[0];
	$hash['minute'] = $d[1];
	$hash['hares'] = explode(',',$hash['hares']);
        $hash['scribes'] = explode(',',$hash['scribes']);
	$hash['hashers'] = db_fetch_all('SELECT * FROM hash_hashers WHERE hash = '.$id);
	if(file_exists('images/hashes/'.$id)) {
		$path = 'images/hashes/'.$id;
		if ($handle = opendir($path)) {
		    while (false !== ($file = readdir($handle))) {
				if(strpos($file,'.jpg') !== false) {
					$hash['photos'][] = $file;
				}

		   }
		closedir($handle);
	       }
        // do something with the file
        }

	if(empty($hash['hashers'])) { $hash['hashers'] = array(); }
	return $hash;

}
