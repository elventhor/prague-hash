<?php

function db_connect()
{
	global $dbConfig;
	global $dbConnection;
	if($dbConnection === null || !mysql_ping($dbConnection))
	{
		$dbConnection = mysql_connect($dbConfig['host'],$dbConfig['user'],$dbConfig['pass']);
		if($dbConnection === false)
		{
			debug('MYSQL DOWN',DEBUG_CRITICAL);
		}
		mysql_select_db($dbConfig['db'],$dbConnection);
	}
	return $dbConnection;
}
function db_fetch_all($sql)
{
        debug($sql,SQL_INFO);
        $db = db_connect();
        $rs = mysql_query($sql,$db);
        $error = mysql_error($db);
        if(!empty($error))
        {
                debug($error,SQL_ERR);
        }
	while($row = mysql_fetch_assoc($rs))
	{
        	$result[] = $row;
	}
	return $result;
}
function db_fetch_row($sql)
{
        debug($sql,SQL_INFO);
        $db = db_connect();
        $rs = mysql_query($sql,$db);
        $error = mysql_error($db);
        if(!empty($error))
        {
                debug($error,SQL_ERR);
        }
	$row = mysql_fetch_assoc($rs);
	return $row;
}
function db_fetch_one($sql)
{
        debug($sql,SQL_INFO);
        $db = db_connect();
        $rs = mysql_query($sql,$db);
	$error = mysql_error($db);
	if(!empty($error))
	{
		debug($error,SQL_ERR);
	}
        $item = mysql_fetch_row($rs);
        return $item[0];
}
function db_insert($sql)
{
	debug($sql,SQL_INFO);
	$db = db_connect();
	if(mysql_query($sql,$db))
	{
		$id = mysql_insert_id($db);
		debug("INSERT ID: $id",SQL_INFO);
		return $id;
	} else
	{
		$error = mysql_error($db);
		debug($error,SQL_ERR);
		return false;
	}
}
function db_update($sql) {
        debug($sql,SQL_INFO);
        $db = db_connect();
        if(mysql_query($sql,$db))
        {
                $id = mysql_affected_rows($db);
                debug("Affected rows: $id",SQL_INFO);
                return $id;
        } else
        {
                $error = mysql_error($db);
                debug($error,SQL_ERR);
                return false;
        }

}
function db_delete($sql) {
	return db_update($sql);
}
