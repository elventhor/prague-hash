<?php /* Smarty version Smarty-3.0.7, created on 2015-11-16 15:07:39
         compiled from "templates/pages/newsItem.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12602114045649d51b6c2908-36347538%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6eea379fa0419b3a5a4f1f2d5aad6a338b9a6e2b' => 
    array (
      0 => 'templates/pages/newsItem.tpl',
      1 => 1447679134,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12602114045649d51b6c2908-36347538',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="span3">
<h1><?php echo $_smarty_tpl->getVariable('newsItem')->value['title'];?>
</h1>
<p>By <i><a href="/hasher/<?php echo $_smarty_tpl->getVariable('newsItem')->value['author'];?>
"><?php echo $_smarty_tpl->getVariable('Hashers')->value[$_smarty_tpl->getVariable('newsItem')->value['author']]['name'];?>
</a></i> at <?php echo $_smarty_tpl->getVariable('newsItem')->value['published'];?>
</p>
<p><?php echo $_smarty_tpl->getVariable('newsItem')->value['content'];?>
</p>
</div>
