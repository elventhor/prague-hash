<?php /* Smarty version Smarty-3.0.7, created on 2015-11-16 15:11:13
         compiled from "templates/pages/about.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6625977655649d5f15c1934-93431352%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85eba33b17b499b72d7d9a1903f6fabdce7ccf6d' => 
    array (
      0 => 'templates/pages/about.tpl',
      1 => 1447679470,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6625977655649d5f15c1934-93431352',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="span9">
<h1>About The Prague Hash House Harriers</h1>
      <div class="hashHeader">What We Do</div>
      <div class="hashText">A friendly group of people meet
up and go on a walk / run.&nbsp;
Typically the walk / run will follow a trail set with flour and involve
a couple of stops to sample excellent Czech beer.&nbsp; We will end at
a pub and have more beers and a meal.&nbsp; It is by no means a
competition and we have everyone from fit runners to gentle walkers
taking part.&nbsp; We welcome people from all over the world, naturally
including Czech, and the main language is English.
      </div>
      <div class="hashHeader">Where We Do It</div>
      <div class="hashText">The walk / run is usually in or
around Prague and will start and finish
at somewhere accessible by public transport.
      </div>
      <div class="hashHeader">How Can I Join?</div>
      <div class="hashText">You can just turn up. Details
about where the next hash will take place
are <a href="mainMenu.hash">here</a>.&nbsp; Also you can join our mailing list by sending an email
to hash@hugh.cz.&nbsp; This will tell you about forthcoming events.<br>
      </div>
      <div class="hashHeader">What Should I Bring?</div>
      <div class="hashText">Suitable running / walking
clothes and enough money for drinks and
optionally a meal.&nbsp; The run / walk&nbsp; costs 80K&#269;, which covers
the cost of beers during the hash.
      </div>
      <div class="hashHeader">History of the Hash</div>
      <div class="hashText">The Hash House Harriers began in
1938 in Malaysia from an idea by someone in the British army who thought it would be fun to combine
jogging with drinking beer. It is based on an old British tradition
called Hounds and Harriers, and since the idea was first thought
of in a Malaysian restaurant called the Hash House, the concept adopted
the name Hash House Harriers. The format has proven to be very popular
indeed and has grown to become a world wide organisation, to such
an extend that at least one hash group can be found in every city in
the world (for an example, you may refer to the global hash website at <a
 href="http://www.gthhh.com">http://www.gthhh.com</a>). </div>
      <div class="hashText">The Prague branch of the Hash House
Harriers (<a href="http://www.praguehhh.com">http://www.praguehhh.com</a>)
began in 1984 and, as all other hash groups around the world, is made
up of a cheery group always eager to meet new people. </div>
      <div class="hashText">The concept is that one person
volunteers to set a run
(trail), and
hence is the harrier for that particular hash. He or she uses flour to
set the trail before the run, occasionally drawing a circle, known as a
check. This is where the faster runners, who get to this check first,
have to look for where the trail continues. Sometime there are false
trails. Once one of the faster runners finds out where the trail
continues, the other runners have usually managed to catch up to the
check, the other faster runners run back to the circle and the whole
group continues along the trail. This is designed to keep the group
together. A typical run lasts approximately one and a half hours and
includes one or more beer breaks in a pub. Those who do not want to run
are informed where the beer breaks and the final beer stop is located,
so that they can walk along their own trail.</div>
      <div class="hashText">This is a great way to discover the
parks in and around a
city, as well
as lots of hidden pubs. </div></div>
