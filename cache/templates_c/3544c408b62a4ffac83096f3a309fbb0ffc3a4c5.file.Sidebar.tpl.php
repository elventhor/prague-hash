<?php /* Smarty version Smarty-3.0.7, created on 2015-11-16 14:42:16
         compiled from "templates/Sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12422736855649cf28ea2267-20682532%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3544c408b62a4ffac83096f3a309fbb0ffc3a4c5' => 
    array (
      0 => 'templates/Sidebar.tpl',
      1 => 1447677640,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12422736855649cf28ea2267-20682532',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Upcoming Hashes</li>
  <!--            <li class="active"><a href="#">Link</a></li>-->
		<?php  $_smarty_tpl->tpl_vars['hash'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('nexthashes')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['hash']->key => $_smarty_tpl->tpl_vars['hash']->value){
?>
              <li><a href="/hash/<?php echo $_smarty_tpl->tpl_vars['hash']->value['id'];?>
">Hash <?php echo $_smarty_tpl->tpl_vars['hash']->value['number'];?>
 <?php if ($_smarty_tpl->tpl_vars['hash']->value['name']){?> / <?php echo $_smarty_tpl->tpl_vars['hash']->value['name'];?>
<?php }?></a></li>
		<?php }} ?>
              <li class="nav-header">Previous Hashes</li>
                <?php  $_smarty_tpl->tpl_vars['hash'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('prevhashes')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['hash']->key => $_smarty_tpl->tpl_vars['hash']->value){
?>
              <li><a href="/hash/<?php echo $_smarty_tpl->tpl_vars['hash']->value['id'];?>
">Hash <?php echo $_smarty_tpl->tpl_vars['hash']->value['number'];?>
 <?php if ($_smarty_tpl->tpl_vars['hash']->value['name']){?> / <?php echo $_smarty_tpl->tpl_vars['hash']->value['name'];?>
<?php }?></a></li>
                <?php }} ?>

              <li class="nav-header">News</li>
                <?php  $_smarty_tpl->tpl_vars['new'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('news')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['new']->key => $_smarty_tpl->tpl_vars['new']->value){
?>
              <li><a href="/news/<?php echo $_smarty_tpl->tpl_vars['new']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['new']->value['title'];?>
</a></li>
		<?php }} ?>
              <li class="nav-header">Info</li>
              <li><a href="/about/">About us</a></li>
              <li><a href="/visitors/">Visitors</a></li>
              <li><a href="/history/">History</a></li>
              <li><a href="/shop/">Haberdashery</a></li>
              <li><a href="/photos/">Photos</a></li>
              <li><a href="/contacts/">Contacts</a></li>
              <li><a href="/links/">Links</a></li>

              <li class="nav-header">Hashers</li>
		<?php  $_smarty_tpl->tpl_vars['hasher'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('hashers')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['hasher']->key => $_smarty_tpl->tpl_vars['hasher']->value){
?>
              <li><a href="/hasher/<?php echo $_smarty_tpl->tpl_vars['hasher']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['hasher']->value['name'];?>
</a></li>
		<?php }} ?>
            </ul>
          </div><!--/.well -->

