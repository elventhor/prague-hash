<?php /* Smarty version Smarty-3.0.7, created on 2015-11-16 15:02:17
         compiled from "templates/pages/hash.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21092044395649d3d9c5a976-19995312%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bfb90314eb988a97d94d59a09e411123e510cea0' => 
    array (
      0 => 'templates/pages/hash.tpl',
      1 => 1447678629,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21092044395649d3d9c5a976-19995312',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="span3">
<!-- Template page for a hash -->
<h1>Hash <?php echo $_smarty_tpl->getVariable('hash')->value['number'];?>
</h1>
<div class="details">
    Date &amp; time: <?php echo $_smarty_tpl->getVariable('hash')->value['date'];?>
<br />
    Location: <?php echo $_smarty_tpl->getVariable('hash')->value['location'];?>
<br />
    Hare(s): <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('hash')->value['hares']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
?><a href="/hasher/<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('Hashers')->value[$_smarty_tpl->tpl_vars['i']->value['id']]['name'];?>
</a> <?php }} ?><br />
    Scribe(s): <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('hash')->value['scribes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
?><a href="/hasher/<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('Hashers')->value[$_smarty_tpl->tpl_vars['i']->value['id']]['name'];?>
</a> <?php }} ?><br />

    Hash Type: <?php echo $_smarty_tpl->getVariable('hash')->value['type'];?>
<br />
</div>
<div class="hashers">Attending hashers: <?php echo $_smarty_tpl->getVariable('hash')->value['hashers'];?>
</div>
<div class="description">Description: <?php echo $_smarty_tpl->getVariable('hash')->value['description'];?>
</div>
<div class="scribe">Scripture: <?php echo $_smarty_tpl->getVariable('hash')->value['scribe'];?>
</div>
<div class="photos">Photos: <?php echo $_smarty_tpl->getVariable('hash')->value['photos'];?>
</div>
</div>
