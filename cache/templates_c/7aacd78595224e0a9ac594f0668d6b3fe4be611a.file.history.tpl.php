<?php /* Smarty version Smarty-3.0.7, created on 2015-11-16 15:06:34
         compiled from "templates/pages/history.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6809686025649d4da0f9743-26867904%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7aacd78595224e0a9ac594f0668d6b3fe4be611a' => 
    array (
      0 => 'templates/pages/history.tpl',
      1 => 1447679165,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6809686025649d4da0f9743-26867904',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="span9">
<h1>
	Prague Hash House Harriers - Early History
</h1>

<div class="hashHeader">Part One</div>
<span class="subhead">THE FIRST TEN YEARS: 1984-1994</span>


<div class="hashText">
	As seen through the eyes of Prague "Hashtorian I " and former GM - <strong>Don
		"Private Rommel" Juries</strong> in October 1994.
</div>

<div class="hashText">
	"I left my future wife behind in Holland about 16 months ago, in 1992,
	to come to Prague. She was quite happy there - good job, company car,
	finally some extra space in our apartment! So I had lots of time when I
	arrived to devote to some other things! That became - work, beer, Czech
	(and some non-Czech) women, and the Prague Hash House Harriers. Oh yes,
	and darts at the British Embassy. But the Hash consumed my life the
	most - first in pursuit of finding someone who ran on it, knew of it,
	knew of a telephone number or bulletin board or anything that might
	lead me to it. All the old contacts had left Prague years before and
	the Hash International group in Bangkok had never been given an
	update.... But five months later, by chance, I happened to meet <strong>Donna
		"Hard Drive" McCormally</strong>, reigning Spiritual Advisor at a party wearing
	a Hash T-Shirt. Never give up, I guess.
</div>

<div class="hashText">
	Soon after, I was given the role of On-Sec, more or less because the
	old On-Sec, <strong>Michael "Igor" Carr</strong>, a "vertically
	challenged" individual, had mysteriously disappeared weeks before
	(rumours say he found a local girlfriend who challenged him
	"horizontally" for a change). A few months later (after Run #194), the
	then Grand Master, <strong>Paddy "Ma's Boy" Keenan</strong>, went
	A.W.O.L. after getting a new job and knocking-up his wife (baby boy, by
	the way, <strong>Stephan "Super Goo" Keenan</strong> - named soon after
	exiting his mother's womb). So I appointed (anointed?) myself as GM and
	began to diligently scour the historical records to see what the Prague
	Hash was (and is) all about... The concept of the Prague Hash came to
	the minds of <strong>Dave "Fantum" Lewis</strong> and <strong>Terri
		"Shadow" Lewis</strong> some time in mid-1984. "Fantum" (I assume from the
	records) was a teacher, or administrator, at the International School
	of Prague within the American Embassy compound, and spread the word to
	his fellow compatriots. This included the likes of <strong>Glenn
		"Hornblower" Johnson</strong> and <strong>Karen "Cheesecake" Johnson</strong>,
	also teachers or administrators at the ISP, who were instrumental in
	getting the momentum started (though, they were shipped out of the
	country only a few months after the Hash really got underway).
</div>

<div class="hashText">Run #1 was held on October 26, 1984, with 18
	runners mainly from Embassy staff of the American, British and Canadian
	Embassies, including about half coming from the ISP itself. With the
	exception of the Centennial and bi-Centennial celebration Runs, the
	average size of the Prague Hash has never changed. In those first years
	under the "old" Czechoslovak regime (read "communist"), local Czechs
	would have been at great personal risk to join - and none did (with one
	incredible exception in 1989). But "western" foreigners (those knowing
	something of the Hash House Harriers from running abroad) were limited
	to Embassy staff or military personnel guarding the compounds. So, the
	Prague Hash was destined to stay small.</div>

<div class="hashText">
	Runs were planned only about once a month. And by the first anniversary
	date, only 14 Hashes had been held. Even with the arrival of the
	ultimate Prague mega-Hasher <strong>Mark "Badger" Erwin</strong> to the
	American Embassy, the pace increased only slightly to 15 in the second
	year. The reasons are unknown, but it's our belief that it may have
	been both to ensure a larger gathering (by giving enough time to
	"market" the idea) and because it would have also been difficult for
	the foreign community to hold such an event. We can only imagine a
	bunch of bummishly dressed foreign nutcases screaming, singing, and
	blowing horns through the city in defiance of those trying to maintain
	"order".
</div>

<div class="hashText">
	By the third anniversary, the pace had picked up to over 30 runs a
	year. Embassy staff tended to rotate in and out of Czechoslovakia about
	every two years, and a whole group of enthusiastic Hashers were to
	arrive late in 1986. The record books are filled with their names
	amongst the middle ranks of those with "Most Runs" - and the Hash
	Debris (write-ups ie Hash Trash!) are filled with the stories of their
	antics. Unfortunately, the founders were about to leave (or had already
	left). The first to go were mega-Hashers <strong>Dan "Texaco"
		Haslip</strong> and his wife <strong>Lou "Spot Check" Haslip</strong> in
	mid-May, 1986. They were to return, however, three years later in 1989,
	just in time for the big party on Run #100. And they now, in 1994,
	stand at numbers two and three respectively on the all-time "Most Runs"
	list, with "Texaco" eventually having taken over the Grand Master's
	role in 1990.
</div>

<div class="hashText">
	The second to go was <strong>Ron "Wanker" Dodoo</strong>. He had joined
	on Run #14 and immediately helped shape the Prague Hash into the image
	of his persona - a true wanker. He basically f--ked-up everything, but
	was the kind of guy you couldn't hate. And he could consume massive
	quantities of alcohol, then holding the record for most "On-Downs"
	during the Hashing ceremony (8) and was alone in the record books for
	most consecutive "On-Downs" (4) until <strong>Dave "Squeaky"
		Gamble</strong> joined him on Run #201. In fact, the Prague Hash was so
	dismayed by "Wanker's" departure in the spring of 1987, that they
	created a Wanker Memorial Award in his honor. It became a piece of
	artwork - every time someone adding something to it that represented
	their Hash Being or something idiotic they did on that run, and was
	displayed in the holder's office until the next Hash. (Unfortunately,
	in the early 1990's, it was awarded to a departing Hasher who packed it
	up and took it to San Francisco forever. It was to be replaced in the
	winter of 1993 by a toilet seat, but that too was packed up to San
	Francisco. Today's award, in 1994, comes in the form of a ski -
	presented by <strong>H.W. "Neptunus" Dijkhuizen</strong> of the Hague
	HHH (the Netherlands) during Run #200 - watch this space to discover
	the whereabouts today of this Wanker Ski award!!Hashtorian II
</div>

<div class="hashText">The last of the big names to get shipped out
	were "Fantum" and "Shadow" in June of 1987 after having dedicated their
	years in Czechoslovakia getting the Prague Hash on it's feet - "Fantum"
	having run 50 of the first 53 Runs ("Shadow" 49). They reappeared only
	once - for a reunion run (#162) two years ago, in 1992, - shocked, I'm
	sure, at the developments of both the Prague Hash and Prague
	itself......</div>

<div class="hashText">
	Fresh blood in the late '80's kept the momentum alive. A real hard-core
	group was developing, led by "Badger" and (better half) <strong>Marilyn
		"Cobbler" Erwin</strong>. Attendance was at its all time peak during the years
	up to the Velvet Revolution. About 25 to 30 Hashers was quite common,
	with 45+ appearing for Run #75 in 1988. But, again, job rotations were
	to hit hard just before the upheaval in Prague in late '89. Dedicated
	Hashers like former Co-GM <strong>Graeme "L'il Nuts" Addy</strong> were
	to leave. <strong>Dan "Mouldy Nutz" Gill, Kari "Madonna"
		Lethonen, Biz "Moo Moo" MacDonald, Barry "Grunt Grunt" MacDonald</strong>...were
	all to go, as well. Unfortunately, right before the Hash Centennial
	Run!
</div>

<div class="hashText">
	The 100th Run, on September 30th, 1989, was to be the first big
	international event for the Prague Hash, and "Badger", "Texaco", and <strong>Dave
		"OD" Pearson</strong> did well to pull it off - even introducing the Good
	Soldier Svejk (the bumbling, but clever fool of Jaroslav Hasek's novel)
	as the mascot, symbolizing the true mentality of PH3ers...... The
	Hashing chapters in Vienna, Budapest, and Warsaw were invited, but were
	limited to Westerners. With the communist regime still hanging on to
	power, "security regulations (made) it impossible for East Bloc
	citizens to enter the British or Canadian Embassies w/o special
	permission". It was best for both sides to avoid each other. Yet, not
	all Easterners were keen on the rules. <strong>Jiri "E.G."
		Petrak</strong> and his wife Hana, after years of Hashing in Baghdad, took the
	risk to pose as Austrians in order to enter the Embassy Clubs where
	most events were held. (They're still around today to tell the stories,
	after having returned from a couple of more years abroad - they do not
	run the hash anymore but are seen sometimes around town - Hashtorian
	II).
</div>

<div class="hashText">
	Only weeks later, both the Prague Hash, and Prague, were thrown into
	chaos when the communists were ousted and Vaclav Havel returned to
	power. "Cobbler" gave birth (the first known PH3 baby) and was out of
	commission for a while. "OD" was to leave shortly after. And "Badger"
	began spending an inordinate amount of time in Munich. Luckily, new
	Embassy rotations, and the very first of the corporate ex-pats brought
	in some new life. But it was up to the likes of "Texaco" and "Spot
	Check" to carry the group, with <strong>Gert "Saurkraut" Guse</strong>,
	Joe "Wireless" Delmonego, Marion "Basshole" Schoni and a few others to
	tidy up the fringes. Disaster struck, however, just before the 150th
	Run in 1991, when the entire Mismanagement team left the country. But a
	nice mix was developing between corporate ex-pats, young entrepreneurs,
	and Embassy staff in Prague, allowing for a constant replenishment of
	Hashers. And, though not to make as big a mark on the Prague Hash as
	those who had left, a new group from around early-1991 (having come in
	during the two year rotational cycle) was to keep the traditions alive.
</div>

<div class="hashText">
	The 150th Run was the biggest event so far in this the first ten years.
	"Basshole" (having just moved to Berlin), European Hashing stars Scoby
	Beazley of the Berlin HHH, and <strong>Detlef "Gigolo"
		Siepmann</strong> (since returned from Hanoi or was it Shanghai?! for Ph3
	550th in 2001!) from Warsaw led the charge of foreign Hash
	representation. Total participants numbered in the low 70's, by far the
	biggest crowd gathered for a Prague Hash in its then seven year
	history. An incredible amount of virgins, too, were initiated to the
	Hashing experience. And it would be these same virgins that were left
	holding the torch after being abandoned shortly after the new year. The
	Prague Hash was entering .....it's "dark ages".
</div>

<div class="hashText">
	It took over two and a half years to wade through the chaos and get to
	the bi-Centennial celebration in May, 1994. <strong>Mari "Egg
		Sucker" Novak</strong> (Prague's first and only Grand Goddess) and <strong>Steve
		"Egg" Kelly</strong> kept the (now not-so) virgins interested, but the new
	Embassy staff just having rotated in were not at all keen on Hashing.
	And the corporate ex-pats and entrepreneurs were engulfed in all the
	changes taking place to the country (or countries, as it soon became).
	For the first time, Czech locals were recruited heavily to try to in
	fill in the gaps and create some stability, but very few were in a
	position to lead the group (or even understand the group). After a
	year, "Egg Sucker" was forced to eradicate the throne in order to
	dedicate herself to her business, with "Ma's Boy" taking control ("Hard
	Drive" attempted a short coup at the time, but it ended with her
	drinking several On-Downs for simply burbling). Though, as noted above,
	"Ma's Boy" too was soon to go A.W.O.L. leaving the lot to myself,
	Private Rommel, "Squeaky", and <strong>Bernie "U2" Fitzpatrick</strong>
	right before the 200th/May1994/!
</div>

<div class="hashText">
	But the bi-Centennial Run went a long way to put the Prague Hash back
	on the map. "Gigolo" and others from the 150th were to return,
	encouraging over 60+ alone to appear from out of town. Hashes as far
	away as Rome and the UK sent representatives. And it stamped the PH3
	with the reputation of being one of the most drunken chapters in
	Europe. <strong>Andrew "Periodical" Davenall</strong> (West London) was
	heard to say it was "one of the most alcoholic weekends he'd spent".
	Ever since, we've had a host of visitors (from Scotland, Australia, and
	Denmark.)
</div>

<div class="hashText">Yet, the numbers of Prague Hashers remains
	small. Some like it that way - intimate and family like, with new
	virgins having replaced those that have either left or faded away. But
	the few newcomers to arrive with some experience can feel the untapped
	potential in this city. Now's the chance to ride on the coat tails of
	Prague Hash's Ten Years of History. Unfortunately, I won't be here to
	see any part of it. I just hope I've left the PH3 a little better than
	I found it...Bye all!"</div>

<div class="hashText">written by Private Rommel aka Don Juries, ex
	GM, 24th October, 1994</div>
</div>
