<?php /* Smarty version Smarty-3.0.7, created on 2015-11-16 12:40:37
         compiled from "templates/Navi.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16732684945649b2a55ae440-14893468%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c21c52578d3c48f86fb06cad90d1618a7befe0c' => 
    array (
      0 => 'templates/Navi.tpl',
      1 => 1447670388,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16732684945649b2a55ae440-14893468',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Prague Hash</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
		<?php if ($_smarty_tpl->getVariable('user')->value['id']){?>
              Logged in as <a href="/hasher/<?php echo $_smarty_tpl->getVariable('user')->value['id'];?>
" class="navbar-link"><?php echo $_smarty_tpl->getVariable('user')->value['name'];?>
</a>
		<?php }else{ ?>
		<a href="/login/">Log in</a> or Facebook
		<?php }?>
            </p>
            <ul class="nav">
		<?php if ($_smarty_tpl->getVariable('admin')->value){?>
              <li class="active"><a href="/admin/">Admin</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
		<?php }?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

