<?php
if(isset($_REQUEST['page'])) {
	$p = $_REQUEST['page'];
	$s = "script/$p.php";
	if(file_exists($s)) { require_once($s); }
	if(!isset($page) && file_exists('templates/pages/'.$p.'.tpl')) {
		$page = $p;
	}
	if(empty($page)) {
		$page = 'error/404';
	}
} else { 
	require_once('script/default.php');
}
$smarty->assignByRef('page',$page);
