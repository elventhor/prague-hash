<?php
if(isset($_REQUEST['form']))
{
	debug("Processing form ".$_REQUEST['form']);
	debug($_REQUEST);
	switch($_REQUEST['form'])
	{
		case "resetPassword":
			$pw = generatePassword();
			$hasher = db_fetch_row('SELECT * FROM hashers WHERE email = "'.escape($_REQUEST['email']).'"');
			if(empty($hasher)) { 
                                $message[] = "ERROR! NO SUCH EMAIL.";
			} else {
				$hash = getpwHash($pw);
				db_update('UPDATE hashers SET pwhash = "'.escape($hash).'" WHERE id = '.$hasher['id']);
		                $subject = "[PRAGUE H3] New password for the Prague H3 website";
				$body = "Your new password is ".$pw."\nnPlease change it from the website!\n";
	                        mail($hasher['name']." <".$hasher['email'].'>',$subject,$body,'From: Prague Hash Mismanagement <hash@elventhor.net>');
				$message[] = "New password has been sent to your email!";
			}
			break;
			//                                         $hash = getpwHash(trim($request['newpass']));


		case "leaveHash":
			db_delete('DELETE FROM hash_hashers WHERE hash = '.intval($_REQUEST['hash_id']).' AND hasher = '.$user['id']);
			break;
		case "joinHash":
			db_insert('INSERT INTO hash_hashers (hash,hasher) VALUES ('.intval($_REQUEST['hash_id']).','.$user['id'].')');
			break;
		case "updateHash":
			foreach($_REQUEST as $k => $v) {
				$hash[$k] = escape(stripslashes($v));
			}
			$hash['scribes'] = implode(',',$_REQUEST['scribes']);
                        $hash['hares'] = implode(',',$_REQUEST['hares']);
			updateHash($hash);
			break;
		case "hash_scribe":
		/*
			<input type="hidden" name="form" value="hash_scribe" />
			<input type="hidden" name="hash_id" value="{$hash.id}" />
			<textarea name="scripture" rows="30" placeholder="Please type your scripture here!">{$hash.scripture}</textarea><br />
			<input type="submit" name="submit" value="Save scripture" />
		*/
			db_update('UPDATE hashes SET scribe = "'.escape($_REQUEST['scripture']).'" WHERE id = "'.intval($_REQUEST['id']).'"');
			break;
		case "createHash":
			if(!$user['admin']) { $message[] = "Only admins can create hashes!"; } else { 
				foreach($_REQUEST as $k => $v) {
                                	$request[$k] = escape(stripslashes($v));
				}
				$request['hares'] = implode(',',$_REQUEST['hares']);
				$hash = createHash($request);
//				if(!empty($hash)) { header("Location: /hash/$hash"); die(); } else { $message[] = "Failed to create hash!";}

			}
			break;
		case "login":
			$user = login($_REQUEST['email'],$_REQUEST['pass']);
			if($user === 0) { $message[] = "No such email"; }
			if($user === 1) { $message[] = "Wrong password"; }
			if(is_array($user)) {
				$_SESSION['user_id'] = $user['id'];
				$message[] = 'Welcome '.$user['name'];
				$_REQUEST['page'] = 'default';
			} else {
				$message[] = "Unknown error";
			}
			break;
		case "editHasher":
			if($_REQUEST['id'] == $user['id'] || $user['admin']) {
				foreach($_REQUEST as $k => $v) {
					db_connect();
					$request[mysql_real_escape_string($k,$dbConnection)] = mysql_real_escape_string(trim(strip_tags($v)),$dbConnection);
				}
				$sql = ('UPDATE hashers SET realname = "'.$request['realname'].'", name = "'.$request['name'].'", email = "'.$request['email'].'", bio = "'.$request['bio'].'", email_hash = "'.intval($request['email_hash']).'", email_news = "'.intval($request['email_news']).'" WHERE id = "'.intval($request['id']).'"');
				$u = db_update($sql);
				if($u) { $message[]= "Updated hasher data"; } else { $message[] = "No data was changed"; }
				if(isset($request['newpass']) && !empty($request['newpass'])) {
					$hash = getpwHash(trim($request['newpass']));
					$u = db_update('UPDATE hashers SET pwhash = "'.$hash.'" WHERE id = "'.intval($request['id']).'"');
					if($u) {
						$message[] = "Updated password!";
					} else {
						$message[] = "Failed to update password";
					}
					
				}
				if(!empty($_FILES['hash_photo'])) {
					if(exif_imagetype($_FILES['hash_photo']['tmp_name']) == IMAGETYPE_JPEG){
							$fn = 'images/hashers/'.intval($request['id']).'.jpg';
						    if(move_uploaded_file($_FILES['hash_photo']['tmp_name'],$fn))
						    {
							exec('convert -resize 400 '.$fn.' '.$fn);
							$message[] = "Updated hash photo!";
						    } else {
							$message[]= 'Failed to copy hash photo!';
						    }
					} else {
						$message[] = "Hash photo must be a jpeg!"; 
					}
				}
			} else {
				$message = "ERROR! Could not update hasher information!"; 
			}
		case "changepass":
			
		break;
		default:
			debug("Unknown form!");
	}
}
