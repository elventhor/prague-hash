<?php
switch($_REQUEST['ajax']) {
	case "uploadPhoto":
		$folder = 'images/hashes/'.intval($_REQUEST['hash_id']);
		mkdir($folder);
		foreach($_FILES as $file) {
 	               if(exif_imagetype($file['tmp_name']) == IMAGETYPE_JPEG){
				$tn = tempnam($folder,'hash-');
				$fn = $tn.'.jpg';
				unlink($tn);
	                        if(move_uploaded_file($file['tmp_name'],$fn))
         	                {
               			        exec('convert -resize 800 '.$fn.' '.$fn);
                        	        $message[] = "Updated hash photo!";
	                        } else {
         	                       $message[]= 'Failed to copy hash photo!';
                	        }
                        } else {
                        	$message[] = "Hash photo must be a jpeg!";
                        }
                }
		ajaxSuccess("Uploaded $cnt photos!");


	case "register":
		$email = escape(trim($_REQUEST['email']));
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			if(email_exists($email)) {
				ajaxFail("Email already exists! Please use reset password or contact admin instead!");
			} else {
				sendRegistrationEmail($email);
				ajaxSuccess("Registration email sent!"); 
			}
		} else {
			ajaxFail("Invalid email!");
		}
		ajaxSuccess("OK",$_REQUEST);
	default:
		ajaxFail("No such backend");
}
